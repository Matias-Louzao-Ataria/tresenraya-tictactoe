import os

def _reiniciar_tablero():
    tablero = [[],[],[]]
    for i in tablero:
        for j in range(3):
            i.append('*')
    
    return tablero

def _mostrar_tablero(tablero):
    conty = 1
    for i in tablero:

        if(conty == 1):
            print(' ',end='')
            for k in range(1,4):
                print(f'{k} ',end='')
            print()
            
        print(conty,end='')

        for j in i:
            print(f'{j} ',end='')

        conty +=1
        print()

def _pedir_numero(str):
    
    while(True) :
        try:
            n = int(input(str))
            if(n <= 0 or n >= 4):
                raise ValueError()
            else:
                return n
        except ValueError as e:
            print('La posición debe de estar entre 1 y 3 ([1,3]).')
        except TypeError as e:
            print('Eso no es un número.')

def _pedir_posicion_ficha(tablero,fichas,ficha_char):
    while(True):
        x = _pedir_numero('Introduce en que columna quieres colocar tu pieza:')
        y = _pedir_numero('Introduce en que fila quieres colocar tu pieza:')

        x-=1
        y-=1

        if(tablero[y][x] == '*'):
            tablero[y][x] = ficha_char
            return fichas+1
        else:
            print('Ya hay una pieza en esa posición escoge otra!')

def _colocar_ficha(tablero,fichas,ficha_char):
    if(fichas >= 3):
        while(True):
            x = _pedir_numero('Introduce la columna de la pieza que quieres mover:')
            y = _pedir_numero('Introduce la fila de la pieza que quieres mover:')
        
            x-=1
            y-=1

            if(tablero[y][x] == ficha_char):
                tablero[y][x] = '*'
                temp = _pedir_posicion_ficha(tablero,fichas,ficha_char)
                print(f'temp {temp}')
                return temp
            else:
                print('Esa no es tu ficha y por lo tanto no puedes moverla! No hagas trampa eh!')
    else:
        temp = _pedir_posicion_ficha(tablero,fichas,ficha_char)
        print(f'temp {temp}')
        return temp

def _guardar(tablero):
    with open(os.environ['HOME']+os.sep+'puntuacionesTresEnRaya.txt','a') as file:
        while(True):
            nombre = input('Introduce tu nombre: ')
            if(not nombre.isspace()):
                break
            else:
                 print('El nombre asociado a la puntuación no puede ser un espacio en blanco!')

        file.write(f'Esta partida la gano :{nombre}')
        file.write('\n')

        conty = 1
        for i in tablero:

            if(conty == 1):
                file.write(' ')
                for k in range(1,4):
                    file.write(f'{k} ')
                file.write('\n')
        
            file.write(f'{conty}')

            for j in i:
                file.write(f'{j} ')

            conty +=1
            file.write('\n')
        
        for i in range(10):
            file.write('-')
        
        file.write('\n')

def main_loop():
    juego = True
    turno = 0

    fichas1 = 0
    fichas2 = 0

    tablero = _reiniciar_tablero()

    print('El jugador 1 utiliza las "X" y el jugador 2 utiliza las "O"')

    while(juego):
        
        ficha_char = 'X' if turno == 0 else 'O'

        _mostrar_tablero(tablero)

        print(f'Turno del jugador:{1 if turno == 0 else 2}')
        
        if(turno == 0):
            fichas1 = _colocar_ficha(tablero,fichas1,ficha_char)
        else:
            fichas2 = _colocar_ficha(tablero,fichas2,ficha_char)

        if((tablero[0][0] == tablero [1][1] and tablero[0][0] == tablero[2][2] and tablero[0][0] != '*') or (tablero[0][0] == tablero [0][1] and tablero[0][0] == tablero[0][2] and tablero[0][0] != '*') or (tablero[1][0] == tablero [1][1] and tablero[1][0] == tablero[1][2] and tablero[1][0] != '*') or (tablero[2][0] == tablero [2][1] and tablero[2][0] == tablero[2][2] and tablero[2][0] != '*') or ((tablero[0][0] == tablero [1][0] and tablero[0][0] == tablero[2][0] and tablero[0][0] != '*') or (tablero[0][1] == tablero [1][1] and tablero[0][1] == tablero[2][1] and tablero[0][1] != '*') or (tablero[0][2] == tablero [1][2] and tablero[0][2] == tablero[2][2] and tablero[0][2] != '*'))):
            juego = False

        if(not juego):
            print(f'Gana el jugador {1 if turno == 0 else 2}')
            _mostrar_tablero(tablero)
            if(input('¿Quieres guardar la puntuación?').lower().find('s') != -1):
                _guardar(tablero)
            if(input('¿Quieres volver a jugar?').lower().find('s') != -1):
                juego = True
                tablero = _reiniciar_tablero()
                fichas1 = 0
                fichas2 = 0
            else:
                print('Gracias por jugar!')

        turno = 1 if turno == 0 else 0

if __name__ == '__main__':
    main_loop()